class Stats:
    # Klasa sluzaca do przetrzymywania, wyswietlania i zapisywania do pliku statystyk algorytmu

    path_to_goal = 0
    cost_of_path = 0
    nodes_expanded = 0
    fringe_size = 0
    max_fringe_size = 0
    search_depth = 0
    max_search_depth = 0
    running_time = 0
    max_ram_usage = 0

    def __init__(self):
        True

    def __str__(self):
        return (
            'path_to_goal: ' + str(self.path_to_goal) +
            '\ncost_of_path: ' + str(self.cost_of_path) +
            '\nnodes_expanded: ' + str(self.nodes_expanded) +
            '\nfringe_size: ' + str(self.fringe_size) +
            '\nmax_fringe_size: ' + str(self.max_fringe_size) +
            '\nsearch_depth: ' + str(self.search_depth) +
            '\nmax_search_depth: ' + str(self.max_search_depth) +
            '\nrunning_time: ' + str(self.running_time) +
            '\nmax_ram_usage: ' + str(self.max_ram_usage)
        )

    def save_to_file(self, filepath):
        f = open(filepath, 'w')
        f.write(self.__str__())
