import math
from State import *


def get_children(state):
    # metoda pobiera stan-rodzica i zwraca stany-dzieci, w ktore da sie przeksztalcic rodzica, przekladajac
    # pusty kafelek na sasiednie miejsca
    # tj. zwraca 2 stany dla pustego kafelka w rogu planszy, 3 dla kafelka przy krawedzi
    # i 4 dla kafelka w srodku planszy

    n = int(len(state.board))  # dlugosc calej tablicy
    d = int(math.sqrt(n))  # dlugosc wierszy i kolumn

    replaces = []  # indeksy pol, na ktore mozna przeniesc pusty kafelek
    directions = []  # kierunki, w ktore mozna przesunac pusty kafelek
    # powyzsze dziala na zasadzie "aby przeniesc pusty kafelek z indeksu poczatkowego do indeksu replace[i],
    # nalezy przesunac go w kierunku directions[i]"

    def apnd(replace, direction):  # prosta funkcja lokalna ograniczajaca powtarzanie kodu
        replaces.append(replace)
        directions.append(direction)

    # zmienne ograniczajace powtarzalnosc kodu
    up = 'Up'
    do = 'Down'
    le = 'Left'
    ri = 'Right'

    index = state.board.index(0)  # indeks pustego kafelka

    # rogi

    # lewy gorny
    if index == 0:
        apnd(d, do)
        apnd(1, ri)
    # prawy gorny
    elif index == d - 1:
        apnd(2 * d - 1, do)
        apnd(d - 2, le)
    # lewy dolny
    elif index == n - d:
        apnd(n - 2 * d, up)
        apnd(n - d + 1, ri)
    # prawy dolny
    elif index == n - 1:
        apnd(n - d - 1, up)
        apnd(n - 2, le)

    # krawedzie

    # gorna
    elif 1 <= index <= d - 2:
        apnd(index + d, do)
        apnd(index - 1, le)
        apnd(index + 1, ri)
    # lewa
    elif index % d == 0:
        apnd(index - d, up)
        apnd(index + d, do)
        apnd(index + 1, ri)
    # dolna
    elif n - d + 1 <= index <= n - 2:
        apnd(index - d, up)
        apnd(index - 1, le)
        apnd(index + 1, ri)
    # prawa
    elif (index + 1) % d == 0:
        apnd(index - d, up)
        apnd(index + d, do)
        apnd(index - 1, le)

    # pola wewnetrzne
    elif d < index < n - d and index % d != 0 and (index + 1) % d != 0:
        apnd(index - d, up)
        apnd(index + d, do)
        apnd(index - 1, le)
        apnd(index + 1, ri)

    children = []
    i = 0
    while i < len(replaces):
        board_list = list(state.board)  # tworzenie kopii tupla w postaci listy
        empty_tile_index = board_list.index(0)  # indeks pustego kafelka
        replace_index = replaces[i]  # indeks kafelka do zamiany z pustym kafelkiem
        board_list[empty_tile_index] = board_list[replace_index]  # zamiana pustego na pelny
        board_list[replace_index] = 0  # zamiana pelnego na pusty
        board_tuple = tuple(board_list)  # kopiowanie listy z powrotem na tupla
        children.append(State(state, board_tuple, directions[i]))  # dodawanie stanu-potomka do zwracanej listy

        i += 1

    return children


def goal_test(state):  # metoda sprawdzajaca, czy elementy w aktualnym stanie sa poukladane (0, 1, ..., n)
    return state.board == tuple(range(len(state.board)))
