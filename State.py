class State:
    # State zawiera dwa tuple:
    # parent, to stan planszy przed przeniesieniem pustego kafelka
    # board, to stan po przeniesieniu
    # dodatkowo pole change_from_parent_to_child okresla, w ktora strone nalezy przesunac pusty kafelek, zeby
    # przejsc ze stanu-rodzica, do stanu-potomka ('Up', 'Down', 'Left', 'Right')

    parent = None
    board = None
    change_from_parent_to_child = None

    def __init__(self, parent, board, change_from_parent_to_child):
        self.parent = parent
        self.board = board
        self.change_from_parent_to_child = change_from_parent_to_child
