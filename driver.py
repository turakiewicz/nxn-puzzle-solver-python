from BFS import *
from DFS import *
import sys


# state = State(None, (1, 2, 5, 3, 4, 0, 6, 7, 8), None)
# state = State(None, (0, 8, 7, 6, 5, 4, 3, 2, 1), None)

def main():
    mode = sys.argv[1]  # okresla, ktory algorytm nalezy uzyc
    argv2 = sys.argv[2]  # pobliera argumenty
    initial_state = argv2.split(",")  # tworzy liste z argumentow oddzielonych przecinkami
    initial_state = tuple(map(int, initial_state))  # konwertuje liste charow na tupla intow

    state = State(None, initial_state, None)

    if mode == 'bfs':
        bfs = BFS(state)
        bfs.work()

    elif mode == 'dfs':
        dfs = DFS(state)
        dfs.work()


# 1, 2, 5, 3, 4, 0, 6, 7, 8
#
state = State(None, (1, 2, 5, 3, 4, 0, 6, 7, 8), None)
dfs1 = DFS(state)
dfs1.work()
