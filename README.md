# NxN Puzzle solver #

Algorithm solving NxN puzzle using breadth and depth first search.
To run the program write in the python command line:
> driver.py <bfs/dfs> <list containing puzzle data (numbers from 0 (empty tile) to n*n-1, > comma-separated)>

The output data is stored in output.txt.