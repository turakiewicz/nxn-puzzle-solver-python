import time
from Stats import *
from statics import *


class DFS:
    frontier = []  # stos stanow do sprawdzenia
    frontier_boards = []  # stos planszy w stanach we frontierze
    # potrzebny do unikniecia wyciagania planszy ze stanow przy kazdym sprawdzeniu czy jest juz we frontierze
    explored = set()  # sprawdzone stany

    stats = Stats()  # pole statystyk

    def __init__(self, initial_state):
        self.frontier.append(initial_state)
        self.frontier_boards.append(initial_state.board)

    def work(self):

        start_time = time.clock()
        debug_licznik = 1

        while self.frontier:
            state = self.frontier.pop()  # pobranie stanu ze stosu
            self.frontier_boards.pop()  # usuniecie planszy
            self.explored.add(state.board)  # dodanie stanu do zbioru sprawdzonych

            # printuple(state.board)
            if debug_licznik % 1000 == 0:
                print debug_licznik
            debug_licznik += 1

            if len(self.frontier) > self.stats.max_fringe_size:  # statystyka maksymalnego rozmiaru frontiera
                self.stats.max_fringe_size = len(self.frontier)

            # odkrywanie drogi ruchow od sprawdzanego stanu (state) do korzenia (initial_state)
            # odkrywanie musi odbywac sie przy kazdej iteracji, zeby mozna bylo zapisac maksymalna glebokosc poszukiwan
            s = state
            solution_path = []
            while True:
                if s.parent is None:  # None to wartosc rodzica stanu inicjalnego
                    break
                solution_path.append(s.change_from_parent_to_child)
                s = s.parent
            if len(solution_path) > self.stats.max_search_depth:
                self.stats.max_search_depth = len(solution_path)  # tutaj nastepuje nadpisanie max glebokosci szukania

            if goal_test(state):  # sukces

                # reszta statystyk
                self.stats.path_to_goal = list(reversed(solution_path))
                self.stats.cost_of_path = len(self.stats.path_to_goal)
                self.stats.fringe_size = len(self.frontier)
                self.stats.search_depth = len(self.stats.path_to_goal)
                self.stats.running_time = time.clock() - start_time

                self.stats.save_to_file("output.txt")

                return

            # w razie nieodnalezienia sciezki dodanie kolejnych stanow do kolejki
            children = get_children(state)
            children = list(reversed(children))
            # frontier_boards = (st.board for st in self.frontier)
            # frontier_boards = []  # frontier zawiera stany, a nie plansze, dlatego potrzeba wyciagnac z niego
            # wszystkie plansze - na pewno nie jest to najoptymalniejsze rozwiazanie
            # for state in self.frontier:
            #    frontier_boards.append(state.board)  # tu plansze przypisywane sa do tablicy

            for child in children:  # a tu sprawdzamy, czy dzieci sprawdzanego stanu sa juz w explored lub frontier
                if (child.board not in self.explored) and (child.board not in self.frontier_boards):
                    self.frontier.append(child)
                    self.frontier_boards.append(child.board)

            self.stats.nodes_expanded += 1  # stat

        # brak rozwiazania
        print 'Failed to find solution'


def printuple(board):
    print str(board[:3])
    print str(board[3:6])
    print str(board[6:])
    print
