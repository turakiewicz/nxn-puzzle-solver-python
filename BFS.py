import Queue
import time
from Stats import *
from statics import *


class BFS:
    frontier = Queue.Queue()  # kolejka stanow do sprawdzenia
    explored = set()  # sprawdzone stany

    stats = Stats()  # pole statystyk

    def __init__(self, initial_state):
        self.frontier.put(initial_state)

    def work(self):

        start_time = time.clock()  # pomiar czasu na poczatku dzialania algorytmu

        while not self.frontier.empty():
            state = self.frontier.get()  # pobranie stanu z kolejki
            self.explored.add(state.board)  # dodanie stanu do zbioru sprawdzonych

            if len(self.frontier.queue) + 1 > self.stats.max_fringe_size:  # stat
                self.stats.max_fringe_size = len(self.frontier.queue) + 1

            if goal_test(state):  # sukces

                # wyluskiwanie rozwiazania ze zdobytych danych
                # State state zawiera State'y-rodzicow, ktore zawieraja State'y-rodzicow itd.
                s = state
                solution_path = []
                while True:
                    if s.parent is None:  # None to wartosc rodzica stanu inicjalnego
                        break
                    solution_path.append(s.change_from_parent_to_child)
                    s = s.parent

                # reszta statystyk
                self.stats.path_to_goal = list(reversed(solution_path))
                self.stats.cost_of_path = len(solution_path)
                self.stats.search_depth = len(solution_path)
                self.stats.max_search_depth = len(solution_path)  # to nie jest prawidlowe rozwiazanie
                self.stats.fringe_size = len(self.frontier.queue)
                self.stats.running_time = time.clock() - start_time

                self.stats.save_to_file('output.txt')  # zapis statystyk do pliku

                return

            # w razie nieodnalezienia sciezki dodanie kolejnych stanow do kolejki
            children = get_children(state)
            for child in children:
                if child.board not in self.explored and child not in tuple(self.frontier.queue):
                    self.frontier.put(child)

            self.stats.nodes_expanded += 1  # stat

        # brak rozwiazania
        print 'Failed to find solution'
